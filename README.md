# Docker, como isso pode mudar seu trabalho

Minicurso ministrado sobre Docker, exibindo suas vantagens e construindo 3 projetos incríveis para sua carreira como desenvolvedor.

## O que é Docker?

Docker, é escrito em GO, que nasceu como um projeto open source da DotCloud, uma empresa de PaaS (Platform as a Service).

O Docker tem como objetivo criar, testar e implementar aplicações em um ambiente separado da máquina original, chamado de container. Dessa forma, o desenvolvedor consegue empacotar o software de maneira padronizada. Isso ocorre porque a plataforma disponibiliza funções básicas para sua execução, como: código, bibliotecas, runtime e ferramentas do sistema.

## Instalação

Use o laboratório do docker online: [Play with Docker](https://labs.play-with-docker.com)

Registre-se, logue e clique em "start"!

## Configuração

Entre na pasta dos códigos:
```docker
cd code
```
Para a criação do blog com [Wordpress](https://hub.docker.com/_/wordpress/)
```bash
cd wordpress
docker-compose up -d
```

Para a criação do blog com [Joomla](https://hub.docker.com/_/joomla/)
```bash
cd joomla
docker-compose up -d
```

Para a criação do CRM com [Odoo](https://hub.docker.com/_/odoo/)
```bash
cd odoo
docker-compose up -d
```

## Autor
1. Péterson Silva([@peterson_silva](https://gitlab.com/peterson_silva/))
